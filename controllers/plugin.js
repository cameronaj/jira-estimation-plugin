exports.index = (req, res) => {
    res.setHeader('Content-Type', 'application/json');

    var obj =
    {
        "name": "Jira Estimation App",
        "description": "Estimation made easy.",
        "key": "com.example.myapp",
        "baseUrl": "https://jira-estimation-plugin.herokuapp.com/",
        "vendor": {
            "name": "Andculture",
            "url": "https://andculture.com"
        },
        "authentication": {
            "type": "none"
        },
        "apiVersion": 1,
        "modules": {
            "jiraProjectPages" : [
                {
                    "url": "/sessions?projectKey=${project.key}",
                    "iconUrl": "https://cdn1.iconfinder.com/data/icons/line-essentials-1/20/44-256.png",
                    "weight": 100,
                    "name": {
                        "value": "Planning Sessions"
                    },
                    "key": "sessions-home"
                }
            ],
            "webItems": [
                {
                    "location": "gh.plan.sprint.actions",
                    "weight": 200,
                    "styleClasses": [
                        "webitem",
                        "system-preset-webitem"
                    ],
                    "url": "/sessions?projectKey=${project.key}",
                    "context": "addon",
                    "target": {
                        "type": "page"
                    },
                    "tooltip": {
                        "value": "Go to Sessions"
                    },
                    "icon": {
                        "width": 16,
                        "height": 16,
                        "url": "https://cdn1.iconfinder.com/data/icons/line-essentials-1/20/44-256.png"
                    },
                    "name": {
                        "value": "Add to Session"
                    },
                    "key": "web-item-add-to-session"
                }
            ],
            "generalPages": [
                {
                    "url": "/sessions",
                    "key": "sessions",
                    "location": "system.top.navigation.bar",
                    "name": {
                        "value": "Estimation Dashboard"
                    }
                }
            ]
        }
    };
    res.send(JSON.stringify(obj));

};